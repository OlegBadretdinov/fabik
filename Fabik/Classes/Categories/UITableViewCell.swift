//
//  UITableViewCell.swift
//  Fabik
//
//  Created by Лена Вышутина on 20.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

extension UITableViewCell {
    class func cellIdentifier() -> String {
        let className: String = NSStringFromClass(self.self)
        if let dotLocation = className.range(of: ".") {
            return String(className[dotLocation.upperBound...])
        } else {
            return className
        }
    }
}
