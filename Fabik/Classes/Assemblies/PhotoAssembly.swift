//
//  PhotoAssembly.swift
//  Fabik
//
//  Created by Лена Вышутина on 12.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import Swinject

class PhotoAssembly: Assembly {
    func assemble(container: Container) {
        container.register(PhotoListNetworkServiceProtocol.self) { (resolver) -> PhotoListNetworkServiceProtocol in
            return PhotoListNetworkService()
        }
        
        container.register(PhotoListStorageProtocol.self) { (resolver) -> PhotoListStorageProtocol in
            return PhotoListStorageService()
        }
        
        container.register(PhotoListManagerProtocol.self) { (resolver) -> PhotoListManagerProtocol in
            let storage = resolver.resolve(PhotoListStorageProtocol.self)
            let network = resolver.resolve(PhotoListNetworkServiceProtocol.self)
            return PhotoListManager(storageService: storage!, networkService: network!)
        }
    }
}
