//
//  PhotoListManager.swift
//  Fabik
//
//  Created by Лена Вышутина on 13.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

protocol PhotoListManagerProtocol {
    func getPhotos(fromId id: Int, limit: Int, completion: @escaping (_ photos: [FPhoto]?, _ error: Error?)->Void)
    func likePhoto(withId id: Int, completion: @escaping (_ isLiked: Bool?, _ error: Error?)->Void)
}

class PhotoListManager: PhotoListManagerProtocol {
    let storage: PhotoListStorageProtocol
    let network: PhotoListNetworkServiceProtocol
    
    init(storageService: PhotoListStorageProtocol, networkService: PhotoListNetworkServiceProtocol) {
        self.storage = storageService
        self.network = networkService
    }
    
    func getPhotos(fromId id: Int, limit: Int, completion: @escaping ([FPhoto]?, Error?) -> Void) {
        self.storage.fetchPhotos(startingWithId: id, limit: limit) { [weak self] (photos) in
            guard let `self` = self else { return }
            if photos.count > 0 {
                completion(photos, nil)
            } else {
                self.loadAndCachePhotos(fromId: id, limit: limit, completion: completion)
            }
        }
    }
    
    fileprivate func loadAndCachePhotos(fromId id: Int, limit: Int, completion: @escaping ([FPhoto]?, Error?) -> Void) {
        self.network.fetchPhotos(lastPhotoId: id, limit: limit) { [weak self] (photos, error) in
            guard let `self` = self else { return }
            if let photos = photos {
                self.storage.savePhotos(photos, completion: {
                    completion(photos, nil)
                })
            } else {
                completion(nil, error)
            }
        }
    }
    
    func likePhoto(withId id: Int, completion: @escaping (Bool?, Error?) -> Void) {
        self.network.likePhoto(withId: id) { [weak self] (status, error) in
            guard let `self` = self else { return }
            if let status = status {
                self.updateCachedPhotoStatus(photoId: id, like: status, completion: {
                    completion(status, nil)
                })
            } else {
                completion(nil, error)
            }
        }
    }
    
    fileprivate func updateCachedPhotoStatus(photoId: Int, like: Bool, completion: @escaping ()->Void) {
        self.storage.fetchPhoto(withId: photoId, completion: { [weak self] (photo) in
            guard let `self` = self else { return }
            if let photo = photo {
                photo.updateLike(like)
                self.storage.savePhoto(photo, completion: completion)
            } else {
                completion()
            }
        })
    }
}
