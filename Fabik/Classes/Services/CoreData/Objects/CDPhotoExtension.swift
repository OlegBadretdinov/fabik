//
//  CDPhotoExtension.swift
//  Fabik
//
//  Created by Лена Вышутина on 13.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit

extension CDPhoto: ManagedObjectProtocol {
    typealias PlainObjectType = FPhoto
    
    var plainObject: FPhoto {
        get {
            return FPhoto(id: Int(self.id), imageUrl: self.imageUrl, title: self.title, subtitle: self.subtitle, liked: self.liked)
        }
    }
}
