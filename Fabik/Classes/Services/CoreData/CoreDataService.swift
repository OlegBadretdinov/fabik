//
//  CoreDataService.swift
//  Fabik
//
//  Created by Лена Вышутина on 13.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import CoreData


class CoreDataService<T>: StorageProtocol where T:PlainObjectsProtocol {
    typealias ObjectType = T
    
    func saveObjects(_ objects: [ObjectType]) {
        let backgroundContext = self.persistentContainer.newBackgroundContext()
        for object in objects {
            if let managedObject = self.fetchManagedObject(withId: object.id, inContext: backgroundContext) {
                backgroundContext.delete(managedObject)
            }
            _ = object.managedObject(context: backgroundContext)
        }
        try? backgroundContext.save()
        self.saveContext()
    }
    
    func fetchObject(withId id: Int) -> ObjectType? {
        return self.fetchManagedObject(withId: id, inContext: nil)?.plainObject as? ObjectType
    }
    
    fileprivate func fetchManagedObject(withId id: Int, inContext context: NSManagedObjectContext?) -> ObjectType.ManagedType? {
        let request = NSFetchRequest<ObjectType.ManagedType>(entityName: String(describing: ObjectType.ManagedType.self))
        request.predicate = NSPredicate(format: "id == \(id)")
        return (try? (context ?? self.persistentContainer.viewContext).fetch(request))?.first
    }
    
    func fetchObjects(startingWithId startId: Int, limit: Int) -> [ObjectType] {
        let request = NSFetchRequest<ObjectType.ManagedType>(entityName: String(describing: ObjectType.ManagedType.self))
        request.predicate = NSPredicate(format: "id > \(startId)")
        request.fetchLimit = limit
        if let managedObjects = try? self.persistentContainer.viewContext.fetch(request) {
            return (managedObjects.map { $0.plainObject } as? [ObjectType]) ?? []
        } else {
            return []
        }
    }
    
    fileprivate lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Fabik")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    fileprivate func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
