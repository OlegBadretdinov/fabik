//
//  PhotoListStorageService.swift
//  Fabik
//
//  Created by Лена Вышутина on 13.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import Foundation

protocol PhotoListStorageProtocol {
    func savePhotos(_ photos: [FPhoto], completion: @escaping ()->Void)
    func fetchPhotos(startingWithId startId: Int, limit: Int, completion: @escaping ([FPhoto])->Void)
    func fetchPhoto(withId id: Int, completion: @escaping (FPhoto?)->Void)
    func savePhoto(_ photo: FPhoto, completion: @escaping ()->Void)
}

class PhotoListStorageService: PhotoListStorageProtocol {
    func fetchPhoto(withId id: Int, completion: @escaping (FPhoto?) -> Void) {
        let store = CoreDataService<FPhoto>()
        let photo = store.fetchObject(withId: id)
        completion(photo)
    }
    
    func savePhoto(_ photo: FPhoto, completion: @escaping () -> Void) {
        let store = CoreDataService<FPhoto>()
        store.saveObjects([photo])
        completion()
    }
    
    func savePhotos(_ photos: [FPhoto], completion: @escaping () -> Void) {
        let store = CoreDataService<FPhoto>()
        store.saveObjects(photos)
        completion()
    }
    
    func fetchPhotos(startingWithId startId: Int, limit: Int, completion: @escaping ([FPhoto]) -> Void) {
        let store = CoreDataService<FPhoto>()
        let photos = store.fetchObjects(startingWithId: startId, limit: limit)
        completion(photos)
    }
    
}
