//
//  PhotoListNetworkService.swift
//  Fabik
//
//  Created by Лена Вышутина on 12.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import ObjectMapper

enum NetworkErrors: Error {
    case unknown
}

protocol PhotoListNetworkServiceProtocol: class {
    func fetchPhotos(lastPhotoId: Int, limit: Int, completion: @escaping (_ photos: [FPhoto]?, _ error: NetworkErrors?)->Void)
    func likePhoto(withId id:Int, completion: @escaping (_ isLiked: Bool?, _ error: NetworkErrors?)->Void)
}

class PhotoListNetworkService: PhotoListNetworkServiceProtocol {
    
    //Не стал делать полноценный сервис. "Примерную" реализацию можно глянуть здесь https://bitbucket.org/OlegBadretdinov/tinkofftest/src/9e57f3ddb533c086c9515cb02196ce932dd4f7e2/TinkoffTestTask/Model/Network/TTTNetworkManager.swift?at=master&fileviewer=file-view-default
    func fetchPhotos(lastPhotoId: Int, limit: Int, completion: @escaping ([FPhoto]?, NetworkErrors?) -> Void) {
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 15) {
            if let path = Bundle.main.path(forResource: "PhotoResponse", ofType: "json"), let str = try? String(contentsOfFile: path) {
                let photos = try! Mapper<FPhoto>().mapArray(JSONString: str)
                completion(photos, nil)
            } else {
                completion([], .unknown)
            }
        }
    }
    
    func likePhoto(withId id: Int, completion: @escaping (Bool?, NetworkErrors?) -> Void) {
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1) {
            completion(true, nil)
        }
    }
}
