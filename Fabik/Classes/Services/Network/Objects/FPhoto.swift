//
//  NWPhoto.swift
//  Fabik
//
//  Created by Лена Вышутина on 12.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import ObjectMapper
import CoreData

class FPhoto: ImmutableMappable, PlainObjectsProtocol {
    func managedObject(context: NSManagedObjectContext) -> CDPhoto {
        let photo = CDPhoto(context: context)
        photo.imageUrl = self.imageUrl
        photo.title = self.title
        photo.subtitle = self.subtitle
        photo.liked = self.liked
        photo.id = Int32(self.id)
        
        return photo
    }
    
    typealias ManagedType = CDPhoto
    
    let id: Int
    let imageUrl: URL?
    let title: String?
    let subtitle: String?
    fileprivate(set) var liked: Bool
    
    required init(map: Map) throws {
        self.id = try map.value("id")
        self.imageUrl = try? map.value("imageUrl", using: URLTransform())
        self.title = try? map.value("title")
        self.subtitle = try? map.value("subtitle")
        self.liked = try map.value("liked")
    }
    
    init(id: Int, imageUrl: URL?, title: String?, subtitle: String?, liked: Bool) {
        self.id = id
        self.imageUrl = imageUrl
        self.title = title
        self.subtitle = subtitle
        self.liked = liked
    }
    
    func updateLike(_ like: Bool) {
        self.liked = like
    }
}
