//
//  Protocols.swift
//  Fabik
//
//  Created by Лена Вышутина on 13.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import Foundation
import CoreData

protocol PlainObjectsProtocol {
    associatedtype ManagedType: ManagedObjectProtocol
    func managedObject(context: NSManagedObjectContext) -> ManagedType
    var id: Int { get }
}

protocol ManagedObjectProtocol: class where Self: NSManagedObject {
    associatedtype PlainObjectType: PlainObjectsProtocol
    var plainObject: PlainObjectType { get }
    var id: Int32 { get }
}

protocol StorageProtocol {
    associatedtype ObjectType: PlainObjectsProtocol
    
    func saveObjects(_ objects: [ObjectType]) 
    func fetchObjects(startingWithId startId: Int, limit: Int) -> [ObjectType]
}
