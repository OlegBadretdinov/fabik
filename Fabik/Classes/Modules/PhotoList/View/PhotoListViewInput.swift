//
//  PhotoListPhotoListViewInput.swift
//  Fabik
//
//  Created by Oleg Badretdinov on 12/02/2018.
//  Copyright © 2018 Fabik. All rights reserved.
//

protocol PhotoListViewInput: class {
    func receiveNewPhotos(_ photos: [FPhoto])
    func updatePhoto(_ photo: FPhoto)
    func showPhotoFetchError(_ error: Error)
}
