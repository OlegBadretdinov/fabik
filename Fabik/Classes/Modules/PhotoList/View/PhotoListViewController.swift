//
//  PhotoListPhotoListViewController.swift
//  Fabik
//
//  Created by Oleg Badretdinov on 12/02/2018.
//  Copyright © 2018 Fabik. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var output: PhotoListViewOutput!
    
    fileprivate var photos: [FPhoto] = []
    fileprivate var photosCouldBeLoaded = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.self.setupTableView()
    }
    
    fileprivate func setupTableView() {
        self.tableView.register(PhotoListPhotoTableViewCell.self, forCellReuseIdentifier: PhotoListPhotoTableViewCell.cellIdentifier())
        self.tableView.register(ActivityIndicatorTableViewCell.self, forCellReuseIdentifier: ActivityIndicatorTableViewCell.cellIdentifier())
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = self.view.frame.size.width
    }
}

extension PhotoListViewController: PhotoListViewInput {
    func showPhotoFetchError(_ error: Error) {
        self.photosCouldBeLoaded = false
        self.tableView.reloadData()
        
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func receiveNewPhotos(_ photos: [FPhoto]) {
        self.photos.append(contentsOf: photos)
        if photos.count == 0 {
            self.photosCouldBeLoaded = false
        }
        self.tableView.reloadData()
    }
    
    //Тут по хорошему необходимо сделать адекватный апдейт по типу
    func updatePhoto(_ photo: FPhoto) {
        if let index = self.photos.index(where: { (p) -> Bool in
            return p.id == photo.id
        }) {
            self.photos[index] = photo
            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
    
    @objc fileprivate func handleLikeButtonTap(_ sender: UIButton) {
        self.output.didLikePressed(onPhoto: self.photos[sender.tag])
    }
}

extension PhotoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.photos.count + (self.photosCouldBeLoaded ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < self.photos.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: PhotoListPhotoTableViewCell.cellIdentifier()) as! PhotoListPhotoTableViewCell
            
            let photo = self.photos[indexPath.row]
            
            cell.likeButton.isSelected = photo.liked
            cell.titleLabel.text = photo.title
            cell.subtitleLabel.text = photo.subtitle
            cell.photoImageView.sd_setImage(with: photo.imageUrl, completed: nil)
            
            cell.likeButton.tag = indexPath.row
            cell.likeButton.addTarget(self, action: #selector(self.handleLikeButtonTap(_:)), for: .touchUpInside)
            
            return cell
        } else {
            return tableView.dequeueReusableCell(withIdentifier: ActivityIndicatorTableViewCell.cellIdentifier())!
        }
    }
}

extension PhotoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let photo = self.photos[indexPath.row]
        self.output.didLikePressed(onPhoto: photo)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isKind(of: ActivityIndicatorTableViewCell.self) {
            self.output.loadNewPhotos(lastLoadedPhoto: self.photos.last)
        }
    }
}
