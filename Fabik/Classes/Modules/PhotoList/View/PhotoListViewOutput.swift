//
//  PhotoListPhotoListViewOutput.swift
//  Fabik
//
//  Created by Oleg Badretdinov on 12/02/2018.
//  Copyright © 2018 Fabik. All rights reserved.
//

protocol PhotoListViewOutput {
    func didSelectPhoto(_ photo: FPhoto)
    func didLikePressed(onPhoto photo: FPhoto)
    func loadNewPhotos(lastLoadedPhoto photo: FPhoto?)
}
