//
//  ActivityIndicatorTableViewCell.swift
//  Fabik
//
//  Created by Лена Вышутина on 19.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import EasyPeasy


class ActivityIndicatorTableViewCell: UITableViewCell {
    fileprivate let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.activityIndicator.startAnimating()
    }
    
    fileprivate func setupSubviews() {
        self.addSubview(self.activityIndicator)
        
        self.activityIndicator.startAnimating()
        
        self.activityIndicator.easy.layout(Center())
    }
}
