//
//  PhotoListPhotoTableViewCell.swift
//  Fabik
//
//  Created by Лена Вышутина on 19.02.2018.
//  Copyright © 2018 Oleg Badretdinov. All rights reserved.
//

import UIKit
import EasyPeasy

class PhotoListPhotoTableViewCell: UITableViewCell {
    
    let photoImageView = UIImageView()
    let titleLabel = UILabel()
    let subtitleLabel = UILabel()
    let likeButton = UIButton()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoImageView.sd_cancelCurrentImageLoad()
        self.photoImageView.image = nil
        self.titleLabel.text = ""
        self.subtitleLabel.text = ""
        self.likeButton.removeTarget(nil, action: nil, for: .allEvents)
        self.likeButton.tag = 0
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupSubviews() {
        self.selectionStyle = .none
        
        self.setupImageView()
        self.setupLikeButton()
        self.setupTitleLabel()
        self.setupSubtitleLabel()
    }
    
    fileprivate func setupImageView() {
        self.photoImageView.clipsToBounds = true
        
        self.photoImageView.contentMode = .scaleAspectFill
        self.photoImageView.backgroundColor = .lightGray
        
        self.addSubview(self.photoImageView)
        
        self.photoImageView.easy.layout([Top(8),
                                         Left(8),
                                         Right(8),
                                         Height(300)
            ])
    }
    
    fileprivate func setupLikeButton() {
        self.likeButton.setImage(#imageLiteral(resourceName: "like"), for: .normal)
        self.likeButton.setImage(#imageLiteral(resourceName: "like_pressed"), for: .selected)
        
        self.addSubview(self.likeButton)
        
        self.likeButton.easy.layout([Top(8).to(self.photoImageView),
                                     Right(16),
                                     Width().like(self.likeButton, .height),
                                     Height(30)
            ])
    }
    
    fileprivate func setupTitleLabel() {
        self.titleLabel.font = UIFont.systemFont(ofSize: 17)
        
        self.addSubview(self.titleLabel)
        
        self.titleLabel.easy.layout([Top(8).to(self.photoImageView),
                                     Left(8),
                                     Right(8).to(self.likeButton)])
    }
    
    fileprivate func setupSubtitleLabel() {
        self.subtitleLabel.font = UIFont.systemFont(ofSize: 15)
        self.subtitleLabel.numberOfLines = 5
        
        self.addSubview(self.subtitleLabel)
        
        self.subtitleLabel.easy.layout([Top(4).to(self.titleLabel),
                                        Top(4).to(self.likeButton),
                                        Left(8),
                                        Right(8).to(self.likeButton),
                                        Bottom(8)])
    }
}
