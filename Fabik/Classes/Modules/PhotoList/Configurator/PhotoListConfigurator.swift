//
//  PhotoListPhotoListConfigurator.swift
//  Fabik
//
//  Created by Oleg Badretdinov on 12/02/2018.
//  Copyright © 2018 Fabik. All rights reserved.
//

import UIKit
import Swinject

class PhotoListModuleConfigurator {
    let assembly = Assembler([PhotoAssembly()])

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? PhotoListViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: PhotoListViewController) {
        
        let router = PhotoListRouter()
        
        let presenter = PhotoListPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = PhotoListInteractor()
        interactor.output = presenter
        interactor.manager = self.assembly.resolver.resolve(PhotoListManagerProtocol.self)
        
        presenter.interactor = interactor
        viewController.output = presenter
    }

}
