//
//  PhotoListPhotoListInitializer.swift
//  Fabik
//
//  Created by Oleg Badretdinov on 12/02/2018.
//  Copyright © 2018 Fabik. All rights reserved.
//

import UIKit

class PhotoListModuleInitializer: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var photolistViewController: PhotoListViewController!

    override func awakeFromNib() {

        let configurator = PhotoListModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: photolistViewController)
    }

}
