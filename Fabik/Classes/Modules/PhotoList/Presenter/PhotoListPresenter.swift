//
//  PhotoListPhotoListPresenter.swift
//  Fabik
//
//  Created by Oleg Badretdinov on 12/02/2018.
//  Copyright © 2018 Fabik. All rights reserved.
//
import Foundation

class PhotoListPresenter: PhotoListModuleInput, PhotoListInteractorOutput {
    weak var view: PhotoListViewInput!
    var interactor: PhotoListInteractorInput!
    var router: PhotoListRouterInput!
    
    
}

extension PhotoListPresenter: PhotoListViewOutput {
    func loadNewPhotos(lastLoadedPhoto photo: FPhoto?) {
        self.interactor.fetchPhotos(lastPhoto: photo) { [weak self] (photos, error) in
            DispatchQueue.main.async {
                guard let `self` = self else { return }
                if let photos = photos {
                    self.view.receiveNewPhotos(photos)
                } else if let error = error {
                    self.view.showPhotoFetchError(error)
                }
            }
        }
    }
    
    func didSelectPhoto(_ photo: FPhoto) {
        DispatchQueue.main.async {
            self.router.presentPhotoDetails(photo)
        }
    }
    
    func didLikePressed(onPhoto photo: FPhoto) {
        self.interactor.likePhoto(photo) { [weak self] (photo, error) in
            DispatchQueue.main.async {
                guard let `self` = self else { return }
                if let photo = photo {
                    self.view.updatePhoto(photo)
                } else if let error = error {
                    self.view.showPhotoFetchError(error)
                }
            }
        }
    }
}
