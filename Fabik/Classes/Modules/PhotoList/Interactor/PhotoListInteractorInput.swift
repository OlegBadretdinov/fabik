//
//  PhotoListPhotoListInteractorInput.swift
//  Fabik
//
//  Created by Oleg Badretdinov on 12/02/2018.
//  Copyright © 2018 Fabik. All rights reserved.
//

import Foundation

protocol PhotoListInteractorInput {
    func fetchPhotos(lastPhoto photo: FPhoto?, completion: @escaping (_ photos: [FPhoto]?, _ error: Error?)->Void)
    func likePhoto(_ photo: FPhoto, completion: @escaping (_ photo: FPhoto?, _ error: Error?)->Void)
}
