//
//  PhotoListPhotoListInteractor.swift
//  Fabik
//
//  Created by Oleg Badretdinov on 12/02/2018.
//  Copyright © 2018 Fabik. All rights reserved.
//
import Foundation

class PhotoListInteractor: PhotoListInteractorInput {
    weak var output: PhotoListInteractorOutput!
    var manager: PhotoListManagerProtocol!
    
    fileprivate var isLoadingPhotos = false

    func fetchPhotos(lastPhoto photo: FPhoto?, completion: @escaping ([FPhoto]?, Error?) -> Void) {
        self.isLoadingPhotos = true
        self.manager.getPhotos(fromId: photo?.id ?? 0, limit: 10) { (photos, error) in
            self.isLoadingPhotos = false
            completion(Array(photos?.prefix(10) ?? []), error)//Чтобы укладываться в наш "limit", тк стабом в нетворке подгружается ~70
        }
    }
    
    func likePhoto(_ photo: FPhoto, completion: @escaping (FPhoto?, Error?) -> Void) {
        self.manager.likePhoto(withId: photo.id, completion: { (liked, error) in
            if let liked = liked {
                photo.updateLike(liked)
                completion(photo, nil)
            } else {
                completion(nil, error)
            }
        })
    }
}
