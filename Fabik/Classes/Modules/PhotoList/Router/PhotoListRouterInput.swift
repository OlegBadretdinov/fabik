//
//  PhotoListPhotoListRouterInput.swift
//  Fabik
//
//  Created by Oleg Badretdinov on 12/02/2018.
//  Copyright © 2018 Fabik. All rights reserved.
//

import Foundation

protocol PhotoListRouterInput {
    func presentPhotoDetails(_ photo: FPhoto)
}
